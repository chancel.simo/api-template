import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Role } from "../../Modules/User/roles/role.entity";
import { DatabaseService } from "./database.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([Role])
    ],
    exports: [DatabaseService],
    providers: [DatabaseService],
})
export class DatabaseModule {
}