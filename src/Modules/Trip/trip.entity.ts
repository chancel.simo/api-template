import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { AppBaseEntity } from "../../Base/base.entity";
import { User } from "../User/user.entity";
import { TripDto } from "./trip.dto";

export enum ParcelType {
    COURRIER = 'courrier',
    COLIS = 'colis',
    VALISE = 'valise',
}

@Entity({ name: 'trip' })
export class Trip extends AppBaseEntity {
    @Column('varchar', { name: "startPlace", length: 255, nullable: false })
    startPlace: string;

    @Column('varchar', { name: "arrivalPlace", length: 255, nullable: false })
    arrivalPlace: string;

    @Column('enum', { name: 'parcelType', enum: ParcelType, nullable: false })
    parcelType: ParcelType;

    @Column('int', { name: 'kilo', nullable: true })
    kilo?: number;

    @Column('datetime', { name: 'startDate', nullable: false })
    startDate: Date;

    @Column('varchar', { name: "userId", length: 36, nullable: false })
    userId: string;

    @ManyToOne(() => User, user => user.trips)
    @JoinColumn({ name: 'userId' })
    user?: User;

    @Column('varchar', { name: 'planeTicketPath', nullable: true })
    planeTicketPath?: string;

    toDto(): TripDto {
        return {
            id: this.id,
            startDate: this.startDate,
            arrivalPlace: this.arrivalPlace,
            parcelType: this.parcelType,
            startPlace: this.startPlace,
            kilo: this.kilo,
            planeTicketPath: this.planeTicketPath,
            userId: this.userId,
            user: this.user ? this.user.toDto() : null,
        };
    }

    fromDto(dto: TripDto) {
        this.id = dto.id;
        this.arrivalPlace = dto.arrivalPlace;
        this.kilo = dto.kilo;
        this.parcelType = dto.parcelType;
        this.startDate = dto.startDate;
        this.startPlace = dto.startPlace;
        this.userId = dto.userId;
        this.planeTicketPath = dto.planeTicketPath;
    }
}