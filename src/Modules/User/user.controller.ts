import { Body, Controller, Get, Param, Post, Req, UseGuards } from "@nestjs/common";
import { Request } from "express";
import { Roles } from "../../Base/decorator/role.decorator";
import { User } from "../../Base/decorator/user.decorator";
import { JwtAuthGuard } from "./Guards/jwt-auth.guard";
import { RolesGuard } from "./Guards/roles-guard";
import { RoleName } from "./roles/user-role.entity";
import { GetUserResponse, LoginModel, LoginResponse, SubscribeRequest, SubscribeResponse, UserDto } from "./user.dto";
import { UserService } from "./user.service";

@Controller('user')
export class UserController {
    constructor(
        private userService: UserService
    ) {

    }

    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(RoleName.USER)
    @Get()
    getUsers() {
        return this.userService.getUsers();
    }

    @Get('/:id')
    getUser(@Param() id: string) {
        return this.userService.getUser(id);
    }

    @Post()
    createOrUpdate(@Body() user: UserDto) {
        console.log("🚀 ~ createOrUpdate ~ user", user)

    }

    @Post('subscribe')
    async subscribeUser(@Body() subscribeRequest: SubscribeRequest): Promise<SubscribeResponse> {
        return await this.userService.subscribe(subscribeRequest);
    }

    @Post('login')
    async loginUser(@Body() request: LoginModel): Promise<LoginResponse> {
        return await this.userService.login(request);
    }

    @Post('refresh-token')
    async refreshToken(@Req() request: Request): Promise<LoginResponse> {
        return await this.userService.refreshToken(request);
    }
}