import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { GetUserResponse, LoginModel, LoginResponse, SubscribeRequest, SubscribeResponse, UserDto } from "./user.dto";
import { User } from "./user.entity";
import * as bcrypt from 'bcrypt';
import { RoleName } from "./roles/user-role.entity";
import { JwtService } from "@nestjs/jwt";
import { Role } from "./roles/role.entity";
import { Request } from "express";

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Role)
        private roleRepository: Repository<Role>,
        private jwtService: JwtService
    ) {

    }

    getUser(id: string) {
        return this.userRepository.findOne({ where: { id: id }, relations: ['userRoles', 'userRoles.role'] });
    }

    getUsers() {
        return this.userRepository.find();
    }

    async subscribe(request: SubscribeRequest): Promise<SubscribeResponse> {
        const subscribeResponse = new SubscribeResponse();
        try {
            if (!request.user)
                throw new Error("request undefined");

            const { password, email } = request.user;
            const response = await this.userRepository.findOne({ where: { email: email }, relations: ['userRoles', 'userRoles.role'] });
            if (!response) {
                const userRole = await this.roleRepository.findOne({ where: { code: RoleName.USER } });
                if (userRole)
                    request.user.userRoles = [
                        { roleId: userRole.id }
                    ];
                const userResponse = this.userRepository.create({ ...request.user });

                userResponse.salt = await bcrypt.genSalt();
                userResponse.password = await bcrypt.hash(password, userResponse.salt);


                const userSave = await this.userRepository.save(userResponse);
                //connect user
                userSave.userRoles = response.userRoles;
                const payload = userSave.toDto();

                const jwt = this.jwtService.sign(payload);
                subscribeResponse.accessToken = jwt;
            } else {
                throw new Error('user already exist');
            }

        } catch (error) {
            subscribeResponse.handleError(error);
            console.log("🚀 ~ subscribe ~ error", error);
        }
        return subscribeResponse;
    }

    async login(request: LoginModel): Promise<LoginResponse> {
        const userResponse = new LoginResponse();
        try {
            const { email, password } = request;

            const checkUser = await this.userRepository.findOne({ where: { email: email }, relations: ['userRoles', 'userRoles.role'] });
            if (!checkUser)
                throw new NotFoundException("email or password incorrect");

            const hashedPassword = await bcrypt.hash(password, checkUser.salt);
            if (checkUser.password === hashedPassword) {

                const payload = checkUser.toDto();
                //gen jwt
                const jwt = this.jwtService.sign(payload);
                userResponse.accessToken = jwt;
                userResponse.success = true;
            }
            else
                throw new NotFoundException("email or password incorrect");
        } catch (error) {
            userResponse.handleError(error);
        }
        return userResponse;
    }

    async refreshToken(request: Request): Promise<LoginResponse> {
        const loginResponse = new LoginResponse();
        try {
            const refreshToken = request.cookies['api-refresh-token'];
            if (!refreshToken)
                throw new Error('invalid request');
            const userResponse = await this.userRepository.findOne({ where: { refreshToken: refreshToken } });
            if (!userResponse)
                throw new Error('user undefined');

            const payload = userResponse.toDto();
            //gen jwt
            const jwt = this.jwtService.sign(payload);
            loginResponse.accessToken = jwt;

            loginResponse.success = true;
        } catch (error) {
            loginResponse.handleError(error);
        }
        return loginResponse;
    }
}