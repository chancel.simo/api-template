import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Environment } from '../../../../environment/environment';
import { UserService } from '../user.service';
import { UserDto } from '../user.dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        private userService: UserService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: Environment.jwt_secret,
        });
    }

    async validate(payload: UserDto) {
        const user = await this.userService.getUser(payload.id);
        if (user)
            return user.toDto();

        throw new UnauthorizedException();
    }
}