import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { RoleDto } from "./role.dto";

export class UserRoleDto {
    id?: string;
    roleId: string;
    role?: RoleDto;
    userId?: string;
}