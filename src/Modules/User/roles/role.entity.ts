import { Column, Entity, OneToMany } from "typeorm";
import { AppBaseEntity } from "../../../Base/base.entity";
import { RoleDto } from "./role.dto";
import { RoleName, UserRole } from "./user-role.entity";

@Entity({ name: 'role' })
export class Role extends AppBaseEntity {
    @Column('enum', { name: 'code', enum: RoleName, default: RoleName.USER, nullable: false })
    code: string;

    @Column('varchar', { name: 'label', nullable: false, length: 30 })
    label: string;

    toDto(): RoleDto {
        return {
            id: this.id,
            code: this.code,
            label: this.label,
        }
    }

    fromDto(dto: RoleDto) {
        this.id = dto.id;
        this.code = dto.code;
        this.label = dto.label;
    }
}