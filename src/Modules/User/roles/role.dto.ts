import { UserRoleDto } from "./user-role.dto";

export class RoleDto {
    id: string;
    label: string;
    code: string;
}