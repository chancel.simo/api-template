import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { GenericResponse } from '../Generics/generic.response';
import { TripDto } from '../Trip/trip.dto';
import { UserRoleDto } from './roles/user-role.dto';


export class UserDto {
    @IsString()
    @Length(36, 36)
    id?: string;

    @IsEmail()
    email: string;
    password: string;
    lastname?: string;
    firstname?: string;
    refreshToken?: string;
    userRoles?: UserRoleDto[];
    trips?: TripDto[];
}

export class LoginModel {
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;
}

export class SubscribeRequest {
    user: UserDto;
}

export class GetUserResponse extends GenericResponse {
    user: UserDto;
}

export class LoginResponse extends GenericResponse {
    accessToken?: string;
}

export class SubscribeResponse extends LoginResponse {
}